function tinhNgayHomQua() {
	var ngay = document.getElementById('ngay').value * 1;
	var thang = document.getElementById('thang').value * 1;
	var nam = document.getElementById('nam').value * 1;
	switch (thang) {
		case 1: {
			if (ngay == 1) {
				document.getElementById('result').innerHTML = `31/12/${nam-1}`;
			} else if (ngay > 1 && ngay <= 31) {
				document.getElementById('result').innerHTML = `${ngay-1}/${thang}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày không hợp lệ";


		}
		case 3: {
			if (ngay > 1 && ngay <= 31) {
				document.getElementById('result').innerHTML = `${ngay-1}/${thang}/${nam}`;
			} else if (ngay == 1) {
				document.getElementById('result').innerHTML = `28/${thang-1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày không hợp lệ";
		}
	}
	switch (thang) {


		case 5:
		case 7:
		case 10:
		case 12: {
			if (ngay > 1 && ngay <= 31) {
				document.getElementById('result').innerHTML = `${ngay-1}/${thang}/${nam}`;
			} else if (ngay == 1) {
				document.getElementById('result').innerHTML = `30/${thang-1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày ko hợp lệ";
		}
	}
	switch (thang) {

		case 4:
		case 6:
		case 9:
		case 11: {
			if (ngay > 1 && ngay <= 30) {
				document.getElementById('result').innerHTML = `${ngay-1}/${thang}/${nam}`;
			} else if (ngay == 1) {
				document.getElementById('result').innerHTML = `1/${thang-1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày ko hợp lệ";
		}
	}
	switch (thang) {
		case 8: {
			if (ngay > 1 && ngay <= 31) {
				document.getElementById('result').innerHTML = `${ngay-1}/${thang}/${nam}`;
			} else if (ngay == 1) {
				document.getElementById('result').innerHTML = `31/${thang-1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày ko hợp lệ";
		}
		case 2: {
			if (ngay > 1 && ngay <= 28) {
				document.getElementById('result').innerHTML = `${ngay-1}/${thang}/${nam}`;
			} else if (ngay == 1) {
				document.getElementById('result').innerHTML = `31/${thang-1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày ko hợp lệ";
		}

	}
}


function tinhNgayMai() {
	var ngay = document.getElementById('ngay').value * 1;
	var thang = document.getElementById('thang').value * 1;
	var nam = document.getElementById('nam').value * 1;
	switch (thang) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10: {
			if (ngay > 0 && ngay < 31) {
				document.getElementById('result').innerHTML = `${ngay+1}/${thang}/${nam}`;
			} else if (ngay == 31) {
				document.getElementById('result').innerHTML = `1/${thang+1}/${nam}`;
			} else document.getElementById('result').innerHTML = "ngày không hợp lệ";
		}
	}
	switch (thang) {
		case 4:
		case 6:
		case 9:
		case 11: {
			if (ngay > 0 && ngay < 30) {
				document.getElementById('result').innerHTML = `${ngay+1}/${thang}/${nam}`;
			} else if (ngay == 30) {
				document.getElementById('result').innerHTML = `1/${thang+1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày không hợp lệ";
		}
	}
	switch (thang) {

		case 2: {
			if (ngay > 0 && ngay < 28) {
				document.getElementById('result').innerHTML = `${ngay+1}/${thang}/${nam}`;
			} else if (ngay == 28) {
				document.getElementById('result').innerHTML = `1/${thang+1}/${nam}`;
			} else
				document.getElementById('result').innerHTML = "ngày ko hợp lệ";
		}
		case 12: {
			if (ngay > 0 && ngay < 31) {
				document.getElementById('result').innerHTML = `${ngay+1}/${thang}/${nam}`;
			} else if (ngay == 31) {
				document.getElementById('result').innerHTML = `1/1/${nam+1}`;
			} else
				document.getElementById('result').innerHTML = "ngày ko hợp lệ";
		}


	}


}

function tinhNgayTrongThang() {
	var thang = document.getElementById('month').value * 1;
	var nam = document.getElementById('year').value * 1;
	var soNgayTrongThang = 0;
	switch (thang) {
		case 2: {
			if (nam % 4 == 0 && nam % 100 != 0) {
				soNgayTrongThang = 29;
				document.getElementById('result1').innerHTML = `Tháng ${thang} năm ${nam} có ${soNgayTrongThang} ngày`;
			} else
				soNgayTrongThang = 28;
			document.getElementById('result1').innerHTML = ` Tháng ${thang} năm ${nam} có ${soNgayTrongThang} ngày`;
		}
	}
	switch (thang) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: {
			soNgayTrongThang = 31;
			document.getElementById('result1').innerHTML = ` Tháng ${thang} năm ${nam} có ${soNgayTrongThang} ngày`;
		}
	}
	switch (thang) {
		case 4:
		case 6:
		case 9:
		case 11: {
			soNgayTrongThang = 30;
			document.getElementById('result1').innerHTML = ` Tháng ${thang} năm ${nam} có ${soNgayTrongThang} ngày`;
		}

	}
}

function docSo() {
	var soCoBaChuSo = document.getElementById('soCoBaChuSo').value * 1;
	var chuSoHangTram = Math.floor(soCoBaChuSo / 100);
	var chuSoHangChuc = Math.floor((soCoBaChuSo / 10) % 10);
	var chuSoHangDonVi = Math.floor((soCoBaChuSo % 100) % 10);
	var chuoi = "";
	switch (chuSoHangTram) {
		case 1: {
			chuoi += "một trăm ";
			break;
		}
		case 2: {
			chuoi += "hai trăm ";
			break;
		}
		case 3: {
			chuoi += "ba trăm ";
			break;
		}
		case 4: {
			chuoi += "bốn trăm ";
			break;
		}
		case 5: {
			chuoi += "năm trăm ";
			break;
		}
		case 6: {
			chuoi += "sáu trăm ";
			break;
		}
		case 7: {
			chuoi += "bảy trăm ";
			break;
		}
		case 8: {
			chuoi += "tám trăm ";
			break;
		}
		case 9: {
			chuoi += "chín trăm ";
			break;
		}
	}
	switch (chuSoHangChuc) {
		case 1: {
			chuoi += "một mươi ";
			break;
		}
		case 2: {
			chuoi += "hai mươi ";
			break;
		}
		case 3: {
			chuoi += "ba mươi ";
			break;
		}
		case 4: {
			chuoi += "bốn mươi ";
			break;
		}
		case 5: {
			chuoi += "năm mươi ";
			break;
		}
		case 6: {
			chuoi += "sáu mươi ";
			break;
		}
		case 7: {
			chuoi += "bảy mươi ";
			break;
		}
		case 8: {
			chuoi += "tám mươi ";
			break;
		}
		case 9: {
			chuoi += "chín mươi ";
			break;
		}
	}
	switch (chuSoHangDonVi) {
		case 1: {
			chuoi += "một";
			break;
		}
		case 2: {
			chuoi += "hai";
			break;
		}
		case 3: {
			chuoi += "ba";
			break;
		}
		case 4: {
			chuoi += "bốn";
			break;
		}
		case 5: {
			chuoi += "năm";
			break;
		}
		case 6: {
			chuoi += "sáu";
			break;
		}
		case 7: {
			chuoi += "bảy";
			break;
		}
		case 8: {
			chuoi += "tám";
			break;
		}
		case 9: {
			chuoi += "chín";
			break;
		}
	}
	document.getElementById('result2').innerHTML = chuoi;
}

function timKhoangCachXaNhat() {
	var toaDoX1 = document.getElementById('x1').value * 1;
	var toaDoY1 = document.getElementById('y1').value * 1;
	var toaDoX2 = document.getElementById('x2').value * 1;
	var toaDoY2 = document.getElementById('y2').value * 1;
	var toaDoX3 = document.getElementById('x3').value * 1;
	var toaDoY3 = document.getElementById('y3').value * 1;
	var sinhVien1 = document.getElementById('sinhvien1').value;
	var sinhVien2 = document.getElementById('sinhvien2').value;
	var sinhVien3 = document.getElementById('sinhvien3').value;
	var toaDoXTruongHoc = document.getElementById('x4').value * 1;
	var toaDoYTruongHoc = document.getElementById('y4').value * 1;
	var khoangCachSinhVien1 = Math.sqrt((Math.pow(toaDoXTruongHoc - toaDoX1, 2)) + (Math.pow(toaDoYTruongHoc - toaDoY1, 2)));
	var khoangCachSinhVien2 = Math.sqrt((Math.pow(toaDoXTruongHoc - toaDoX2, 2)) + (Math.pow(toaDoYTruongHoc - toaDoY2, 2)));
	var khoangCachSinhVien3 = Math.sqrt((Math.pow(toaDoXTruongHoc - toaDoX3, 2)) + (Math.pow(toaDoYTruongHoc - toaDoY3, 2)));
	if (khoangCachSinhVien1 > khoangCachSinhVien2 && khoangCachSinhVien1 > khoangCachSinhVien3) {
		document.getElementById('result3').innerHTML = `Sinh Vien xa truong nhat ${sinhVien1}`;
	} else if (khoangCachSinhVien2 > khoangCachSinhVien1 && khoangCachSinhVien2 > khoangCachSinhVien3) {
		document.getElementById('result3').innerHTML = `Sinh Vien xa truong nhat ${sinhVien2}`;
	} else
		document.getElementById('result3').innerHTML = `Sinh Vien xa truong nhat: ${sinhVien3}`;

}